#!/bin/bash

#首先安装esp-open-sdk，在上级目录，与ESP8266App同级.
#git clone http://github.com/pfalcon/esp-open-sdk
#安装下面的库后，执行make.

apt-get install git autoconf build-essential gperf bison flex texinfo libtool libncurses5-dev wget gawk python-serial libexpat-dev

#For 32-bit OS.
#apt-get install libc6-dev-i386 

#For 64-bit OS.
apt-get install libc6-dev-amd64

apt-get install libtool
apt-get install libssl-dev

#Get SDK.
#latest SDK download from http://bbs.espressif.com.

#wget -O esp_iot_sdk_v0.9.3_14_11_21_patch1.zip https://github.com/esp8266/esp8266-wiki/raw/master/sdk/esp_iot_sdk_v0.9.3_14_11_21_patch1.zip
#unzip esp_iot_sdk_v0.9.3_14_11_21.zip

#wget -O lib/libc.a https://github.com/esp8266/esp8266-wiki/raw/master/libs/libc.a
#wget -O lib/libhal.a https://github.com/esp8266/esp8266-wiki/raw/master/libs/libhal.a
#wget -O include.tgz https://github.com/esp8266/esp8266-wiki/raw/master/include.tgz
#tar -xvzf include.tgz

#download from http://mirror.bjtu.edu.cn/gnu/
#and from ftp://gcc.gnu.org/pub/gcc/infrastructure/
#and copy to esp-open-sdk/crosstool-NG/.build/tarballs

#binutils-2.24.tar.gz
wget http://mirror.bjtu.edu.cn/gnu/binutils/binutils-2.24.tar.gz

#cloog-0.18.1.tar.gz
wget ftp://gcc.gnu.org/pub/gcc/infrastructure/cloog-0.18.1.tar.gz

#gcc-4.8.2.tar.gz
wget http://mirror.bjtu.edu.cn/gnu/gcc/gcc-4.8.2/gcc-4.8.2.tar.bz2

#gdb-7.5.1.tar.bz2gmp-5.1.3.tar.xz
wget http://mirror.bjtu.edu.cn/gnu/gdb/gdb-7.5.1.tar.bz2

#gmp-5.1.3.tar.xz
wget http://mirror.bjtu.edu.cn/gnu/gmp/gmp-5.1.3.tar.bz2

#isl-0.12.2.tar.bz2
wget ftp://gcc.gnu.org/pub/gcc/infrastructure/isl-0.12.2.tar.bz2

#mpc-1.0.2.tar.gz
wget http://mirror.bjtu.edu.cn/gnu/mpc/mpc-1.0.2.tar.gz

#mpfr-3.1.2.tar.xz
wget http://mirror.bjtu.edu.cn/gnu/mpfr/mpfr-3.1.2.tar.bz2

#newlib-2.0.0.tar.gz
