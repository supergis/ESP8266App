#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include "gpsnmea.h"

#define INDENT_SPACES "  "

int GetInfo(void)
{
    char line[GPSNMEA_MAX_LENGTH];
    while (fgets(line, sizeof(line), stdin) != NULL) {
        printf("%s", line);
        switch (gpsnmea_sentence_id(line, false)) {
            case GPSNMEA_SENTENCE_RMC: {
                struct gpsnmea_sentence_rmc frame;
                if (gpsnmea_parse_rmc(&frame, line)) {
                    printf(INDENT_SPACES "$xxRMC: raw coordinates and speed: (%d/%d,%d/%d) %d/%d\n",
                            frame.latitude.value, frame.latitude.scale,
                            frame.longitude.value, frame.longitude.scale,
                            frame.speed.value, frame.speed.scale);
                    printf(INDENT_SPACES "$xxRMC fixed-point coordinates and speed scaled to three decimal places: (%d,%d) %d\n",
                            gpsnmea_rescale(&frame.latitude, 1000),
                            gpsnmea_rescale(&frame.longitude, 1000),
                            gpsnmea_rescale(&frame.speed, 1000));
                    printf(INDENT_SPACES "$xxRMC floating point degree coordinates and speed: (%f,%f) %f\n",
                            gpsnmea_tocoord(&frame.latitude),
                            gpsnmea_tocoord(&frame.longitude),
                            gpsnmea_tofloat(&frame.speed));
                }
                else {
                    printf(INDENT_SPACES "$xxRMC sentence is not parsed\n");
                }
            } break;

            case GPSNMEA_SENTENCE_GGA: {
                struct gpsnmea_sentence_gga frame;
                if (gpsnmea_parse_gga(&frame, line)) {
                    printf(INDENT_SPACES "$xxGGA: fix quality: %d\n", frame.fix_quality);
                }
                else {
                    printf(INDENT_SPACES "$xxGGA sentence is not parsed\n");
                }
            } break;

            case GPSNMEA_SENTENCE_GST: {
                struct gpsnmea_sentence_gst frame;
                if (gpsnmea_parse_gst(&frame, line)) {
                    printf(INDENT_SPACES "$xxGST: raw latitude,longitude and altitude error deviation: (%d/%d,%d/%d,%d/%d)\n",
                            frame.latitude_error_deviation.value, frame.latitude_error_deviation.scale,
                            frame.longitude_error_deviation.value, frame.longitude_error_deviation.scale,
                            frame.altitude_error_deviation.value, frame.altitude_error_deviation.scale);
                    printf(INDENT_SPACES "$xxGST fixed point latitude,longitude and altitude error deviation"
                           " scaled to one decimal place: (%d,%d,%d)\n",
                            gpsnmea_rescale(&frame.latitude_error_deviation, 10),
                            gpsnmea_rescale(&frame.longitude_error_deviation, 10),
                            gpsnmea_rescale(&frame.altitude_error_deviation, 10));
                    printf(INDENT_SPACES "$xxGST floating point degree latitude, longitude and altitude error deviation: (%f,%f,%f)",
                            gpsnmea_tofloat(&frame.latitude_error_deviation),
                            gpsnmea_tofloat(&frame.longitude_error_deviation),
                            gpsnmea_tofloat(&frame.altitude_error_deviation));
                }
                else {
                    printf(INDENT_SPACES "$xxGST sentence is not parsed\n");
                }
            } break;

            case GPSNMEA_SENTENCE_GSV: {
                struct gpsnmea_sentence_gsv frame;
                if (gpsnmea_parse_gsv(&frame, line)) {
                    printf(INDENT_SPACES "$xxGSV: message %d of %d\n", frame.msg_nr, frame.total_msgs);
                    printf(INDENT_SPACES "$xxGSV: sattelites in view: %d\n", frame.total_sats);
                    for (int i = 0; i < 4; i++)
                        printf(INDENT_SPACES "$xxGSV: sat nr %d, elevation: %d, azimuth: %d, snr: %d dbm\n",
                            frame.sats[i].nr,
                            frame.sats[i].elevation,
                            frame.sats[i].azimuth,
                            frame.sats[i].snr);
                }
                else {
                    printf(INDENT_SPACES "$xxGSV sentence is not parsed\n");
                }
            } break;

            case GPSNMEA_INVALID: {
                printf(INDENT_SPACES "$xxxxx sentence is not valid\n");
            } break;

            default: {
                printf(INDENT_SPACES "$xxxxx sentence is not parsed\n");
            } break;
        }
    }

    return 0;
}

/* vim: set ts=4 sw=4 et: */
