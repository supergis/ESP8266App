#ifndef GPSNMEA_H
#define GPSNMEA_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>
#include <time.h>
#include <math.h>

#define GPSNMEA_MAX_LENGTH 80

enum gpsnmea_sentence_id {
    GPSNMEA_INVALID = -1,
    GPSNMEA_UNKNOWN = 0,
    GPSNMEA_SENTENCE_RMC,
    GPSNMEA_SENTENCE_GGA,
    GPSNMEA_SENTENCE_GSA,
    GPSNMEA_SENTENCE_GLL,
    GPSNMEA_SENTENCE_GST,
    GPSNMEA_SENTENCE_GSV,
};

struct gpsnmea_float {
    int_least32_t value;
    int_least32_t scale;
};

struct gpsnmea_date {
    int day;
    int month;
    int year;
};

struct gpsnmea_time {
    int hours;
    int minutes;
    int seconds;
    int microseconds;
};

struct gpsnmea_sentence_rmc {
    struct gpsnmea_time time;
    bool valid;
    struct gpsnmea_float latitude;
    struct gpsnmea_float longitude;
    struct gpsnmea_float speed;
    struct gpsnmea_float course;
    struct gpsnmea_date date;
    struct gpsnmea_float variation;
};

struct gpsnmea_sentence_gga {
    struct gpsnmea_time time;
    struct gpsnmea_float latitude;
    struct gpsnmea_float longitude;
    int fix_quality;
    int satellites_tracked;
    struct gpsnmea_float hdop;
    struct gpsnmea_float altitude; char altitude_units;
    struct gpsnmea_float height; char height_units;
    int dgps_age;
};

enum gpsnmea_gll_status {
    GPSNMEA_GLL_STATUS_DATA_VALID = 'A',
    GPSNMEA_GLL_STATUS_DATA_NOT_VALID = 'V',
};

enum gpsnmea_gll_mode {
    GPSNMEA_GLL_MODE_AUTONOMOUS = 'A',
    GPSNMEA_GLL_MODE_DPGS = 'D',
    GPSNMEA_GLL_MODE_DR = 'E',
};

struct gpsnmea_sentence_gll {
    struct gpsnmea_float latitude;
    struct gpsnmea_float longitude;
    struct gpsnmea_time time;
    char status;
    char mode;
};

struct gpsnmea_sentence_gst {
    struct gpsnmea_time time;
    struct gpsnmea_float rms_deviation;
    struct gpsnmea_float semi_major_deviation;
    struct gpsnmea_float semi_minor_deviation;
    struct gpsnmea_float semi_major_orientation;
    struct gpsnmea_float latitude_error_deviation;
    struct gpsnmea_float longitude_error_deviation;
    struct gpsnmea_float altitude_error_deviation;
};

enum gpsnmea_gsa_mode {
    GPSNMEA_GPGSA_MODE_AUTO = 'A',
    GPSNMEA_GPGSA_MODE_FORCED = 'M',
};

enum gpsnmea_gsa_fix_type {
    GPSNMEA_GPGSA_FIX_NONE = 1,
    GPSNMEA_GPGSA_FIX_2D = 2,
    GPSNMEA_GPGSA_FIX_3D = 3,
};

struct gpsnmea_sentence_gsa {
    char mode;
    int fix_type;
    int sats[12];
    struct gpsnmea_float pdop;
    struct gpsnmea_float hdop;
    struct gpsnmea_float vdop;
};

struct gpsnmea_sat_info {
    int nr;
    int elevation;
    int azimuth;
    int snr;
};

struct gpsnmea_sentence_gsv {
    int total_msgs;
    int msg_nr;
    int total_sats;
    struct gpsnmea_sat_info sats[4];
};

/**
 * Calculate raw sentence checksum. Does not check sentence integrity.
 */
uint8_t gpsnmea_checksum(const char *sentence);

/**
 * Check sentence validity and checksum. Returns true for valid sentences.
 */
bool gpsnmea_check(const char *sentence, bool strict);

/**
 * Determine talker identifier.
 */
bool gpsnmea_talker_id(char talker[3], const char *sentence);

/**
 * Determine sentence identifier.
 */
enum gpsnmea_sentence_id gpsnmea_sentence_id(const char *sentence, bool strict);

/**
 * Scanf-like processor for NMEA sentences. Supports the following formats:
 * c - single character (char *)
 * d - direction, returned as 1/-1, default 0 (int *)
 * f - fractional, returned as value + scale (int *, int *)
 * i - decimal, default zero (int *)
 * s - string (char *)
 * t - talker identifier and type (char *)
 * T - date/time stamp (int *, int *, int *)
 * Returns true on success. See library source code for details.
 */
bool gpsnmea_scan(const char *sentence, const char *format, ...);

/*
 * Parse a specific type of sentence. Return true on success.
 */
bool gpsnmea_parse_rmc(struct gpsnmea_sentence_rmc *frame, const char *sentence);
bool gpsnmea_parse_gga(struct gpsnmea_sentence_gga *frame, const char *sentence);
bool gpsnmea_parse_gsa(struct gpsnmea_sentence_gsa *frame, const char *sentence);
bool gpsnmea_parse_gll(struct gpsnmea_sentence_gll *frame, const char *sentence);
bool gpsnmea_parse_gst(struct gpsnmea_sentence_gst *frame, const char *sentence);
bool gpsnmea_parse_gsv(struct gpsnmea_sentence_gsv *frame, const char *sentence);

/**
 * Convert GPS UTC date/time representation to a UNIX timestamp.
 */
int gpsnmea_gettime(struct timespec *ts, const struct gpsnmea_date *date, const struct gpsnmea_time *time_);

/**
 * Rescale a fixed-point value to a different scale. Rounds towards zero.
 */
static inline int_least32_t gpsnmea_rescale(struct gpsnmea_float *f, int_least32_t new_scale)
{
    if (f->scale == 0)
        return 0;
    if (f->scale == new_scale)
        return f->value;
    if (f->scale > new_scale)
        return (f->value + ((f->value > 0) - (f->value < 0)) * f->scale/new_scale/2) / (f->scale/new_scale);
    else
        return f->value * (new_scale/f->scale);
}

/**
 * Convert a fixed-point value to a floating-point value.
 * Returns NaN for "unknown" values.
 */
static inline float gpsnmea_tofloat(struct gpsnmea_float *f)
{
    if (f->scale == 0)
        return NAN;
    return (float) f->value / (float) f->scale;
}

/**
 * Convert a raw coordinate to a floating point DD.DDD... value.
 * Returns NaN for "unknown" values.
 */
static inline float gpsnmea_tocoord(struct gpsnmea_float *f)
{
    if (f->scale == 0)
        return NAN;
    int_least32_t degrees = f->value / (f->scale * 100);
    int_least32_t minutes = f->value % (f->scale * 100);
    return (float) degrees + (float) minutes / (60 * f->scale);
}

#ifdef __cplusplus
}
#endif

#endif /* GPSNMEA_H */

/* vim: set ts=4 sw=4 et: */
