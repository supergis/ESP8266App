/*********************************************
 *UDPserver for ESP8266.
 *Author: openthings@163.com. 
 *copyright&GPL V2.
 *Last modified 2014-10-20.
 **********************************************/

#ifndef __WIX_UDPSERVER_H__
#define __WIX_UDPSERVER_H__

//#define SERVER_PORT 8000
//#define SERVER_SSL_PORT 443

//======================================================
//查询TCP/IP服务器。
void ICACHE_FLASH_ATTR xc_cmd_CUDPSERVERQ(uint8_t id, char *pParam);
//启动TCP/IP服务器。
void ICACHE_FLASH_ATTR xc_cmd_CUDPSERVER(uint8_t id, char *pParam);
//关闭TCP/IP服务器。
void ICACHE_FLASH_ATTR xc_cmd_CUDPSCLOSE(uint8_t id, char *pParam);

//======================================================
//启动udpserver在缺省端口-8266。
void ICACHE_FLASH_ATTR udpserver_start();
//启动udpserver在指定端口。
void ICACHE_FLASH_ATTR	 udpserver_init(uint32 port);
//UDP数据接收。
void ICACHE_FLASH_ATTR udpserver_recv(void *arg, char *pusrdata, unsigned short len);
//UDP数据发送。
void ICACHE_FLASH_ATTR	 udpserver_send(char *pbuf, unsigned short length);

#endif