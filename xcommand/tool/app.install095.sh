#!/bin/bash
CWD=$(pwd)
OUTPUTBIN=$CWD"/app.wixserver.bin"

FILE_00000="../firmware/095/0x00000.bin"
FILE_40000="../firmware/095/0x40000.bin"
FILE_7C000="../../../esp-open-sdk/esp_iot_sdk_v0.9.5/bin/esp_init_data_default.bin"
FILE_7E000="../../../esp-open-sdk/esp_iot_sdk_v0.9.5/bin/blank.bin"

FILE_00000_SIZE=$(stat -c%s $FILE_00000)
FILE_40000_SIZE=$(stat -c%s $FILE_40000)
FILE_7C000_SIZE=$(stat -c%s $FILE_7C000)
FILE_7E000_SIZE=$(stat -c%s $FILE_7E000)

echo "build blank flash image"
IMAGESIZE=$((0x7E000 + FILE_7E000_SIZE))
#thank igr for this tip
dd if=/dev/zero bs=1 count="$IMAGESIZE" conv=notrunc | LC_ALL=C tr "\000" "\377" > "$OUTPUTBIN"

echo "patch image with bins"
dd if="$FILE_00000" of="$OUTPUTBIN" bs=1 seek=0 count="$FILE_00000_SIZE" conv=notrunc
JUMP=$((0x40000))
dd if="$FILE_40000" of="$OUTPUTBIN" bs=1 seek="$JUMP" count="$FILE_40000_SIZE" conv=notrunc  
JUMP=$((0x7C000))
dd if="$FILE_7C000" of="$OUTPUTBIN" bs=1 seek="$JUMP" count="$FILE_7C000_SIZE" conv=notrunc  
JUMP=$((0x7E000))
dd if="$FILE_7E000" of="$OUTPUTBIN" bs=1 seek="$JUMP" count="$FILE_7E000_SIZE" conv=notrunc  
 
echo ">>build image finished.========================"

echo "Upload firmware to ESP8266..." 
echo "Don't forget set GPIO0 to LOW."

echo "writing firmware [app.wixserver.bin] to 0x00000..."
sudo python esptool.py --port /dev/ttyUSB1 write_flash 0x00000 ./app.wixserver.bin

echo ">>Install firmware Finished.==================="
echo "Don't forget unset GPIO0 to HIGH."

