/*********************************************
 *Flow control over WiFi and UART for ESP8266.
 *Author: openthings@163.com. 
 *copyright & GPL V2.
 *Last modified 2014-10-23.
 ********************************************/
#ifndef __XC_CMD_H__
#define __XC_CMD_H__

//最多的指令数量。
#define xc_cmdNum   50

//指令结构与函数定义。
typedef struct
{
	char *cmdName;													//指令名称。
	int8_t cmdLen;														//指令长度。
	void (*cmdFunc)(uint8_t id,char *pPara);			//指令执行函数。
}	cmd_functionType;

int8_t ICACHE_FLASH_ATTR dataStrCpy(void *pDest, const void *pSrc, int8_t maxLen);	//字符串拷贝。

void xc_cmd_Null(uint8_t id, char *pPara);							//测试可用性。
void xc_cmd_About(uint8_t id, char *pPara);						//显示系统固件信息。
void xc_cmd_GMR(uint8_t id, char *pPara);						//显示固件的版本号。

void xc_cmd_CHIPID(uint8_t id, char *pPara);					//显示模块芯片ID.
void xc_cmd_HEAP(uint8_t id, char *pPara);						//显示可用内存信息。

void xc_cmd_MACQ(uint8_t id, char *pPara);						//显示固件的MAC地址。
void xc_cmd_MAC(uint8_t id, char *pPara);						//设置固件的MAC地址。

void xc_cmd_MACAPQ(uint8_t id, char *pPara);				//显示固件的MAC地址。
void xc_cmd_MACAP(uint8_t id, char *pPara);					//设置固件的MAC地址。

void xc_cmd_PROMIS(uint8_t id, char *pPara);					//设置混杂模式是否打开。

void xc_cmd_Chain(uint8_t id, char *pPara);						//链接到外部串口控制器。
void xc_cmd_setupE(uint8_t id, char *pPara);					//设置是否回显输入指令。
void xc_cmd_RST(uint8_t id, char *pPara);							//重置模块。

void xc_cmd_onCmd(char* pCMD);										//执行控制指令。可以来自于串口或WiFi。

#endif
