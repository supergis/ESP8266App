/*********************************************
 *TCP configration for ESP8266.
 *Author: openthings@163.com. 
 *copyright&GPL V2.
 *Last modified 2014-10-20.
 **********************************************/

#include "c_types.h"
#include "user_interface.h"
#include "xc_version.h"
//#include "version.h"
#include "espconn.h"
#include "mem.h"
#include "xc.h"
#include "osapi.h"
#include "driver/uart.h"
#include "xc_cmd.h"
#include "xc_cmd_tcp.h"

extern uint8_t xc_wifiMode;
extern int8_t dataStrCpy(void *pDest, const void *pSrc, int8_t maxLen);

extern cmd_functionType cmd_fun[xc_cmdNum];

//连接句柄，全局共享。
extern struct espconn esp_conn_server;
extern struct espconn esp_conn_client;
extern uint16_t server_timeover;
extern uint16_t server_port;

/*********************************************
 *查询IP地址。
 * 获取STATION模式的IP地址。
 * add get station ip and ap.
 *********************************************/
void ICACHE_FLASH_ATTR xc_cmd_CIPQ(uint8_t id,char *pParam)
{
	struct ip_info pTempIp;
	char temp[64];

	if (wifi_get_ip_info(STATION_IF, &pTempIp)) {
		os_sprintf(temp, "%d.%d.%d.%d\r\n", IP2STR(&pTempIp.ip));
		sendInfoAll(temp);
	} else	{	//AP模式，没有STATION的IP地址。
		sendInfoAll("WiFiMode error.\r\n");
	}
}

/*********************************************
 *设置IP地址。
 * 包括：IP,gateway,mask.
 *********************************************/
void ICACHE_FLASH_ATTR xc_cmd_CIP(uint8_t id, char *pParam)
{
	//sendInfoAll("SET IP unavailable.");
	struct ip_info pTempIp;

	char delims[] = ",";
	char *ip = strtok( pParam, delims );
	char *gateway =  strtok( NULL, delims );
	char *mask =  strtok( NULL, delims );

	//有效的IP至少包括：0.0.0.0
	if (strlen(ip) < 7) {
		sendInfoAll("IP error.\r");
		return;
	}
	if (strlen(gateway) < 7) {
		sendInfoAll("gateway error.\r");
		return;
	}
	if (strlen(mask) < 7) {
		sendInfoAll("mask error.\r");
		return;
	}

	//设置IP地址。
	wifi_get_ip_info(0x00, &pTempIp);
	pTempIp.ip.addr = ipaddr_addr(ip);
	pTempIp.gw.addr = ipaddr_addr(gateway);
	pTempIp.netmask.addr = ipaddr_addr(mask);
		
	if (wifi_set_ip_info(0x00, &pTempIp))	{
		os_printf("%d.%d.%d.%d\r\n",IP2STR(&pTempIp.ip));
		os_printf("%d.%d.%d.%d\r\n",IP2STR(&pTempIp.gw));
		os_printf("%d.%d.%d.%d\r\n",IP2STR(&pTempIp.netmask));
	} else {
		sendInfoAll("set ip error.\r");
		return;
	}
}

/*********************************************
 *查询AP的IP地址。
 *AP模式时，有AP和客户端两个IP地址，可能还不在一个网段中。
 * 因此，应该分为两个函数。
 *********************************************/
void ICACHE_FLASH_ATTR xc_cmd_CIPAPQ(uint8_t id, char *pParam)
{
	struct ip_info pTempIp;
	char temp[64];

	wifi_get_ip_info(0x01, &pTempIp);
	os_sprintf(temp, "%d.%d.%d.%d\r", IP2STR(&pTempIp.ip));
	sendInfoAll(temp);
	//mdState = m_gotip;
}

/*********************************************
 *设置AP的IP地址。
 *********************************************/
void ICACHE_FLASH_ATTR xc_cmd_CIPAP(uint8_t id, char *pParam)
{
	struct ip_info pTempIp;
	int8_t len;
	char ipTemp[64];

	if(strlen(pParam) < 7) {
		sendInfoAll("IP ERROR\r\n");
		return;
	}

	wifi_get_ip_info(0x01, &pTempIp);

	strcpy(ipTemp, pParam);
	pTempIp.ip.addr = ipaddr_addr(ipTemp);

	if (wifi_set_ip_info(0x01, &pTempIp))	{
		os_printf("%d.%d.%d.%d\r\n",IP2STR(&pTempIp.ip));	
	}else{
		sendInfoAll("error, set ip of ap.");
		return;
	}	
}


/*******************************************************
  * @brief  Setup commad of Timeout.
  * @param  id: commad id number
  * @param  pParam: AT input param
  * @retval None
  ******************************************************/
void ICACHE_FLASH_ATTR xc_cmd_CIPSTOQ(uint8_t id,char *pParam)
{
	char temp[32];
	os_sprintf(temp, "%s:%d\r\n", cmd_fun[id].cmdName, server_timeover);
	sendInfoAll(temp);
}

/*******************************************************
  * @brief  Setup commad of Timeout.
  * @param  id: commad id number
  * @param  pParam: AT input param
  * @retval None
  ******************************************************/
void ICACHE_FLASH_ATTR xc_cmd_CIPSTO(uint8_t id, char *pParam)
{
	char temp[32];
	uint16_t timeOver;
	
	timeOver = atoi(pParam);
	if (timeOver>28800)	{
		sendInfoAll("Timeout must<28800.");
		return;
	}
	
	server_timeover = timeOver;
	espconn_regist_time(&esp_conn_server, server_timeover, 0);
	
	os_sprintf(temp, "%s:%d\r", cmd_fun[id].cmdName, server_timeover);
	sendInfoAll(temp);
}



