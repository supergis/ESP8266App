/*********************************************
 *GPIO Operation for ESP8266.
 *Author: openthings@163.com. 
 *copyright&GPL V2.
 *Last modified 2014-10-20.
 **********************************************/

#ifndef __XC_CMD_GPIO_H__
#define __XC_CMD_GPIO_H__

//GPIO端口读写操作。
void xc_cmd_CGPIOQ(uint8_t id, char* pParam);
void xc_cmd_CGPIO(uint8_t id, char* pParam);

#endif