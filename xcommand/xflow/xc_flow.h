/*********************************************
 *Flow control over WiFi and UART for ESP8266.
 *Author: openthings@163.com. 
 *copyright & GPL V2.
 *Last modified 2014-10-23.
 ********************************************/
/*
 *数据流的处理，从中分离出指令和数据。
 */

#ifndef __XC_FLOW_H__
#define __XC_FLOW_H__

void xc_flow_BufferClean();

void sendInfo(char* strInfo);
void sendInfoln(char* strInfo);
void sendInfoAll(char* strInfo) ;

void xc_flow_onUart(uint8_t a_uart);
void xc_flow_onUartReceiving(char* struart);	//接收到串口硬中断来的数据。
void xc_flow_onWifiReceiving(char* strwifi);	//接收到WiFi传来的数据。
void xc_flow_onFlowFilter(char* strdata);	//接收数据的统一处理，分离出指令和数据。

void xc_flow_onData(char* strdata);			//除指令外的其他数据处理，一般做透明交换。

void xc_flow_sendtoWifi(char* strdata);		//串口数据转发到WiFi端口。
void xc_flow_sendtoUart(char* strdata);		//WiFi端口数据转发到串口。

#endif
