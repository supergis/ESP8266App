/*********************************************
 *TCP configration for ESP8266.
 *Author: openthings@163.com. 
 *copyright&GPL V2.
 *Last modified 2014-10-20.
 **********************************************/

#ifndef __XC_CMD_TCP_H__
#define __XC_CMD_TCP_H__

//设置和查询作为网卡的IP地址，一般由接入点自动分配。
void ICACHE_FLASH_ATTR xc_cmd_CIPQ(uint8_t id, char *pParam);
void ICACHE_FLASH_ATTR xc_cmd_CIP(uint8_t id, char *pParam);
//wifi_set_macaddr

//设置和查询作为接入点的IP地址。
void ICACHE_FLASH_ATTR xc_cmd_CIPAPQ(uint8_t id, char *pParam);
void ICACHE_FLASH_ATTR xc_cmd_CIPAP(uint8_t id, char *pParam);


//查询和设置超时参数。
void ICACHE_FLASH_ATTR xc_cmd_CIPSTOQ(uint8_t id,char *pParam);
void ICACHE_FLASH_ATTR xc_cmd_CIPSTO(uint8_t id, char *pParam);

#endif