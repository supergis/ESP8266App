/*********************************************
 *TCPServer for ESP8266.
 *Author: openthings@163.com. 
 *copyright&GPL V2.
 *Last modified 2014-10-20.
 **********************************************/

#ifndef __WIX_TCPCLIENT_H__
#define __WIX_TCPCLIENT_H__

#define REMOTE_SERVER_PORT 8000
#define REMOTE_SERVER_SSL_PORT 443

//作为客户端的当前连接信息。
void ICACHE_FLASH_ATTR xc_cmd_CIPCLIENTQ(uint8_t id, char *pParam);
//启动作为客户端的连接。
void ICACHE_FLASH_ATTR xc_cmd_CIPCLIENT(uint8_t id, char *pParam);
//关闭作为客户端的连接。
void ICACHE_FLASH_ATTR xc_cmd_CIPCLOSE(uint8_t id, char *pParam);

//========================================================
//启动客户端，连接到服务器。
void tcpclient_connect(char* server, uint32 port);
void ICACHE_FLASH_ATTR	 	tcpclient_recon(void *arg, sint8 err);

//网络事件回调函数。
static void ICACHE_FLASH_ATTR tcpclient_connect_cb(void *arg);
static void ICACHE_FLASH_ATTR tcpclient_recon_cb(void *arg, sint8 errType);
static void ICACHE_FLASH_ATTR tcpclient_sent_cb(void *arg);
static void ICACHE_FLASH_ATTR tcpclient_discon_cb(void *arg);

//网络发送/接收函数
void ICACHE_FLASH_ATTR		tcpclient_recv(void *arg, char *pusrdata, unsigned short length);
void ICACHE_FLASH_ATTR		tcpclient_send(char *pbuf, unsigned short length);

//断开客户端到服务器的连接。
void ICACHE_FLASH_ATTR	 	tcpclient_discon(void *arg);

#endif
