/*********************************************
 *UDPclient for ESP8266.
 *Author: openthings@163.com. 
 *copyright&GPL V2.
 *Last modified 2014-10-20.
 **********************************************/

#ifndef __WIX_UDPCLIENT_H__
#define __WIX_UDPCLIENT_H__

//======================================================
//UDP，作为客户端的当前连接信息。
void ICACHE_FLASH_ATTR xc_cmd_CUDPCLIENTQ(uint8_t id, char *pParam);
//启动作为客户端的连接。
void ICACHE_FLASH_ATTR xc_cmd_CUDPCLIENT(uint8_t id, char *pParam);
//关闭作为客户端的连接。
void ICACHE_FLASH_ATTR xc_cmd_CUDPCLOSE(uint8_t id, char *pParam);

//======================================================
//启动udpserver在指定端口。
void udpclient_connect(char* udpserver, uint32 port);
void ICACHE_FLASH_ATTR	 udpclient_recon(void *arg, sint8 err);

//UDP数据接收。
void ICACHE_FLASH_ATTR udpclient_recv(void *arg, char *pusrdata, unsigned short len);
//UDP数据发送。
void ICACHE_FLASH_ATTR	 udpclient_send(char *pbuf, unsigned short length);

#endif
