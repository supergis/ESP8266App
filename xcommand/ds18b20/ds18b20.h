#ifndef __DS18B20_H__
#define __DS18B20_H__

#include "ets_sys.h"
#include "osapi.h"
#include "gpio.h"

void ds_init(int gpio);
int ds_search(uint8_t *addr);
void select(const uint8_t rom[8]);
void reset_search();
uint8_t reset(void);
void write(uint8_t v, int power);
void write_bit(int v);
uint8_t read();
int read_bit(void);
uint8_t crc8(const uint8_t *addr, uint8_t len);

#endif
