#!/bin/bash

#创建分支：git fork
#检到本地：git clone
#查看状态：git remote -v
#添加上游：git remote add upstream origin-url
#更新上游：git fetch upstream
#检出自己的fork：git checkout master
#合并上游更新：git merge upstream/master
#更新自己的库：git push

echo "===================================================="
echo "2-merge esp_mqtt."
cd esp_mqtt
git remote add upstream https://github.com/tuanpmt/esp_mqtt.git
git fetch upstream
git merge upstream/master
cd ..

echo "===================================================="
echo "2B-merge espduino."
cd espduino
git remote add upstream https://github.com/tuanpmt/espduino.git
git fetch upstream
git merge upstream/master
cd ..

echo "===================================================="
echo "3-merge esp_mqtt_oled."
cd esp_mqtt_oled
git remote add upstream https://github.com/nathanchantrell/esp_mqtt_oled.git
git fetch upstream
git merge upstream/master
cd ..

#From Espressif offically.
echo "===================================================="
echo "4-merge esp_iot_rtos_sdk_lib."
cd esp_iot_rtos_sdk
git remote add upstream https://github.com/espressif/esp_iot_rtos_sdk_lib.git
git fetch upstream
git merge upstream/master
cd ..

echo "===================================================="
echo "4A-merge esp_iot_rtos_sdk."
cd esp_iot_rtos_sdk
git remote add upstream https://github.com/espressif/esp_iot_rtos_sdk.git
git fetch upstream
git merge upstream/master
cd ..

echo "===================================================="
echo "4B-merge esp8266_at."
cd esp8266_at
git remote add upstream https://github.com/espressif/esp8266_at.git
git fetch upstream
git merge upstream/master
cd ..

echo "===================================================="
echo "4C-merge low_power_espressif."
cd low_power_voltage_measurement
git remote add upstream https://github.com/EspressifSystems/low_power_voltage_measurement.git
git fetch upstream
git merge upstream/master
cd ..
#End from Espressif offically.

echo "===================================================="
echo "6-merge micropython for esp8266."
cd micropython
git remote add upstream https://github.com/micropython/micropython.git
git fetch upstream
git merge upstream/master
cd ..

echo "===================================================="
echo "7-merge nodemcu."
cd nodemcu/nodemcu-firmware
git remote add upstream https://github.com/nodemcu/nodemcu-firmware.git
git fetch upstream
git merge upstream/master
cd ../../

echo "===================================================="
echo "8-merge nodelua."
cd nodelua
git remote add upstream https://github.com/haroldmars/nodelua.git
git fetch upstream
git merge upstream/master
cd ..

echo "===================================================="
echo "9-merge frankenstein."
cd esp8266-frankenstein
git remote add upstream https://github.com/nekromant/esp8266-frankenstein.git
git fetch upstream
git merge upstream/master
cd ..

