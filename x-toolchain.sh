#!/bin/bash

echo "================================="
echo "Build toolchain link for ESP8266."
echo "================================="

cd ./esp-open-sdk/xtensa-lx106-elf/bin

ln xtensa-lx106-elf-addr2line xt-addr2line
ln xtensa-lx106-elf-ar xt-ar
ln xtensa-lx106-elf-as xt-as
ln xtensa-lx106-elf-c++filt xt-c++filt
ln xtensa-lx106-elf-gprof xt-gprof
ln xtensa-lx106-elf-ld xt-ld
ln xtensa-lx106-elf-nm xt-nm
ln xtensa-lx106-elf-objcopy xt-objcopy
ln xtensa-lx106-elf-objdump xt-objdump
ln xtensa-lx106-elf-ranlib xt-ranlib
ln xtensa-lx106-elf-readelf xt-readelf
ln xtensa-lx106-elf-size xt-size
ln xtensa-lx106-elf-strings xt-strings
ln xtensa-lx106-elf-strip xt-strip
ln xtensa-lx106-elf-g++ xt-xc++
ln xtensa-lx106-elf-gcc xt-xcc

cd ..
echo "================================="
echo "Build toolchain link  is OK."
echo "================================="


