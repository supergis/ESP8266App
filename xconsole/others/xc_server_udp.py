#!/usr/bin/env python
 
from socket import *
from time import ctime

#HOST = '192.168.199.179'
HOST = '127.0.0.1'
PORT = 9999 
ADDR = (HOST, PORT) 

udpServerSock = socket(AF_INET,SOCK_DGRAM)
udpServerSock.bind(ADDR)

while True:
	print "wait for message..."
	data,addr = udpServerSock.recvfrom(1024)
	
	udpServerSock.sendto('[%s] %s'% (ctime(),data),addr)
	print 'received and returned to...',addr,data

udpServerSock.close()
