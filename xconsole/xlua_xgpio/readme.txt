#******************************************
#This file created by OpenThings.
#Author: openthings@163.com. copyright&GPL V2.
#Last modified 2014-12-10.
#******************************************

WiFiSensor无线传感器，DS18B20/DS18S20温度测量模块。
---------------------------
使用onewire单总线结构，可在一个GPIO口上串接多个传感器。
将filelist中的文件全部烧入，重新启动即可。

包括：nodeMCU基础固件及刷写工具，LUA动态固件及刷写工具。
烧写工具：
1、首先烧写nodemcu基础固件，需要GPIO0接地。
2、上载动态固件的lua代码，使用./tool_lua/下：sudo python install.py $lfall

支持数据接口：
USB，通过模拟串口返回数据，以#开始的温度数据。
tcpserver,运行在服务器模式，
httpserver,
tcpclient,
httpclient,


测试程序：
tcpclient
tcpserver

