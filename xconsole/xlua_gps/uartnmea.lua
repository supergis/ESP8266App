local xgps = {}

function xgps.start()
    uart.on("data", "\r", 
        function(data)
    		if string.sub(data,1,1)=="\n" then
    		  data = string.sub(data,2)
    		end
    		local sub_str = string.sub(data, 1, 6)
    		--print("CMD:"..data)
    		if sub_str=="$GNGLL" or sub_str=="$GPGGA" or  sub_str=="$GNRMC" then
        		print("LOC: "..data)
           		gserver.send(data)
    		end
 
    		if string.sub(data,1,4)=="quit" then 
              uart.on("data")
              print("now in lua console.")
            end 
    	end, 0)
end

return xgps
