local xserver = {}

gconn=nil

function xserver.tohtml(conn,str)
    conn:send("HTTP/1.1 200 OK\n\n")
    conn:send("<html><body>")
    conn:send(str.."<BR>")
    conn:send("</html></body>")
end

function xserver.send(str)
    if gconn~=nil then
        gconn:send(str)
    else
        print("gconn==nil")
        print(wifi.sta.getip())
        --print("Please connect to IP: "..wifi.sta.getip())
    end
end

function xserver.onlisten(conn)
	print("wifi onlisten...")
	gconn=conn
	
	print("gconn=",gconn)
	conn:on("receive", function(conn, pl) 
		print("REV:",pl)
		if (conn==nil) 	then print("conn is nil.") end
	end)
	end

function xserver.begin()
    if wifi.sta.getip()~=nil then
    	print("gps_server start listen.IP:"..wifi.sta.getip()..":8080,HEAP:"..node.heap())
    	srv=net.createServer(net.TCP)
    	srv:listen(8080,xserver.onlisten)
    else
        print("IP is nil,please connect ap.")
    end
end

function xserver.start()
    tmr.alarm(0,5000, 0,xserver.begin)
end

return xserver
