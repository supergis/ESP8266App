--Set Wifi mode and AP params.
--Author: openthings@163.com. copyright&GPL V2.
--Last modified 2014-11-28.

print("Set Wifi Mode to STATIONAP.")
wifi.setmode(wifi.STATION)

print("Set Wifi AP...")
wifi.sta.config("HiWiFi_SMT","87654321")

print("Current IP:")
print(wifi.sta.getip())

