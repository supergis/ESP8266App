--LUA examples for nodemcu.
--Author: openthings@163.com. copyright&GPL V2.
--Last modified 2014-11-15.

print("===========================================")
print("N     N  OO  DDDDDD  EEEEE    CCCC  U     U")
print("N N   N O  O   D   D E  E    C      U     U")
print("N   N N O  O   D   D E E      C      U     U")
print("N     N  OO  DDDDDD  EEEEE    CCCC   UUUUU ")
print("-------------------------------------------")
print("A lua for nodemcu, by openthings@163.com   ")
print("===========================================")
