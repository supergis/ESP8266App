--LUA examples for nodemcu.
--Author: openthings@163.com. copyright&GPL V2.
--Last modified 2014-11-15.

srv=net.createServer(net.TCP)

srv:listen(80,
	function(conn)
		conn:on("receive",
			function(conn,payload)
    			print(payload)
    			conn:send("<h1> Hello, NodeMcu.</h1>")
  			end
		)
	end
)

