local xfile = {}

function xfile.list()
	l = file.list();
    for k,v in pairs(l) do
      print("file:"..k..", size:"..v)
    end
end

function xfile.view(name)
  file.open(name)
  repeat
    local line=file.readline()
    if line then line=(string.gsub(line,"\n","")) print(line) end
  until not line
  file.close()
end

function xfile.removeall()
	l = file.list();
    for k,v in pairs(l) do
      print("delete:"..k..", size:"..v) 
      file.delete(k)
    end
end

return xfile
