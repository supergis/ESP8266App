--LUA examples for nodemcu.
--Author: openthings@163.com. copyright&GPL V2.
--Last modified 2014-11-15.

id=0
sda=1
scl=0

-- initialize i2c, set pin1 as sda, set pin0 as scl
i2c.setup(id,sda,scl,i2c.SLOW)

-- user defined function: read from reg_addr content of dev_addr
function read_reg(dev_addr, reg_addr)
  i2c.start(id)
  i2c.address(id, dev_addr ,i2c.TRANSMITTER)
  i2c.write(id,reg_addr)
  i2c.stop(id)
  i2c.start(id)
  i2c.address(id, dev_addr,i2c.RECEIVER)
  c=i2c.read(id,1)
  i2c.stop(id)
  return c
end

-- get content of register 0xAA of device 0x77
reg = read_reg(0x77, 0xAA)
pirnt(string.byte(reg))
