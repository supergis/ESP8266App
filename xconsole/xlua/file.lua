--LUA examples for nodemcu.
--Author: openthings@163.com. copyright&GPL V2.
--Last modified 2014-11-15.

-- open 'init.lua' in 'a+' mode
file.open("init.lua", "a+")

-- write 'foo bar' to the end of the file
file.write('foo bar')

file.flush()
file.close()
