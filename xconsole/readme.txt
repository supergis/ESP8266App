#====================================================
XConsole, For ESP8266.
基于python的、可扩展的串口和网络控制程序。
支持通过串口、TCP客户端、UDP客户端三种方式登录到模块系统。

Any question, contact: OpenThings@163.com
#====================================================
说明：
xcon_uart.py：串口登录控制程序。
xcon_tcp.py：tcp登录控制程序。
xcon_udp.py：udp登录控制程序。
xc_serial.py：串口IO操作封装函数。
xc_server_tcp.py：tcpserver可用于调试。
xc_server_udp.py：udpserver可用于调试。
xc_client_tcp.py：tcpclient可用于调试。
xc_client_udp.py：udpclient可用于调试。
#====================================================

>>软件安装(Ubuntu)：
sudo apt-get install python-pip
sudo pip install pyserial

>>指令集：XCMD

>>通过串口执行指令：
连接ESP8266模块到串口。
使用dmesg |grep /dev/ttyUSB查找连接的设备名称。
修改xcon_uart中的串口设备名称。
执行python xcon_uart.py。
输入@，如果显示OK说明已经连接成功。

>>通过WiFi执行指令：
给ESP8266模块加电(3.3V,可支持230ma的电源)。
搜索AP，找到ESPXXXX的无线网络，连接。
修改xcon_udp.py的IP地址为192.168.4.1,端口为8266。
执行python xcon_udp.py。

>>配置外部AP：
输入@+CWJAP=ap_ssid,ap_password，不需加引号。
等待数秒，输入@+CIP?，如果返回非0,已经连接成功。
如果失败，输入@+CWJAP?，返回AP信息，检查是否有误。
输入@+CWLAP，可扫描可用的AP，返回字符串列表。
无线操作需要一定时间，中间应稍微等待再输入下一指令。

>>连接到当前局域网络：
重新连接到新的IP地址，可执行所有的XCMD指令。

