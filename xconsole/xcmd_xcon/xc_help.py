#!/usr/bin/env python
#coding=utf-8

#******************************************
#This file for XCMD, created by OpenThings.
#Using this to automate the test procedure.
#Author: openthings@163.com. copyright&GPL V2.
#Last modified 2014-11-13.
#******************************************

strCMDList=[\
#====================================================
#获取版本等帮助信息。
"@：测试是否支持XCMD。",
"@?：返回固件帮助信息。以后可以返回指令的帮助信息。",
"@+GMR：返回固件版本号。",
"@+CHIPID：显示芯片ID调试信息。",
"@+HEAP：显示可用内存信息。	",
"@+MAC?：查询MAC地址。",
"@+MAC：设置MAC地址。",
"@+MACAP?：查询AP MAC地址。",
"@+MACAP：设置AP MAC地址。",

#====================================================
#运行环境信息。
"@@：链接到串口控制外部设备。	//TODO:",
"@E：是否回显。				//TODO:",
"@D：是否输出调试信息。		//TODO:",
"@+PROMIS：设置混杂模式是否打开。",
"@+SLEEP：深度休眠。			//TODO:",
"@+RST：系统重置。",

#====================================================
#WiFi工作模式设置。
#1-Station，2-softAP，3-softAP+Station
"@+CWMODE?：返回WiFi的工作模式。",
"@+CWMODE=1/2/3：设置WiFi的工作模式。",
"@+CWCHANNEL?：返回WiFi的工作频道。",
"@+CWCHANNEL=0～13：设置WiFi的工作频道。",
	
#====================================================
#设置无线路由器连接信息。
"@+CWLAP：扫描可用的热点。",
"@+CWJAP?：返回接入点SSID和PWD。",
"@+CWJAP=AP,PWD：设置接入点SSID和PWD。",
"@+CWQAP：退出当前AP的连接。",

#====================================================
#softAP设置信息。
"@+CWSAP?：作为AP的参数查询。",
"@+CWSAP=SSID,PWD,CHANNEL,AUTHMODE：作为AP的参数设置。",
"@+CWLIF：查看已接入设备的IP。",

#====================================================
#IP相关的设置。
#包括客户端和AP的IP地址，连接信息，建立到服务器端的连接等。
"@+CIP?：查询IP地址（STATION模式）。",
"@+CIP=*.*.*.*：设置IP地址（STATION模式）。",
"@+CIPAP?：查询softAP的IP地址（AP/STA+AP模式）。",
"@+CIPAP=*.*.*.*：设置softAP的IP地址（AP/STA+AP模式）。",

#====================================================
#TCP客户端设置。
"@+CIPCLIENT?：查看连接的IP客户端状态。",
"@+CIPCLIENT=*.*.*.*,PORT：建立TCP客户端连接到远端服务器。",
"@+CIPCLOSE：关闭到远端服务器的连接。",

#====================================================
#IP服务器设置，包括超时设置、启动服务器等。
"@+CIPSTO?，查询TCP服务器的超时时长。",
"@+CIPSTO=*，设置TCP服务器的超时时长。",
"@+CIPSERVER?，查询TCP服务器状态。",
"@+CIPSERVER=PORT，建立TCP服务器。",
"@+CIPSCLOSE，关闭服务器。",

#====================================================
#UDP客户端设置。
"@+CUDPCLIENT?：查看连接的客户端IP地址。",
"@+CUDPCLIENT=*.*.*.*,PORT：建立TCP客户端连接到远端服务器。",
"@+CUDPCLOSE：关闭到远端服务器的连接。",

#====================================================
#UDP服务器设置。
"@+CUDPSERVER?：查询UDP服务器。",
"@+CUDPSERVER=PORT：建立UDP服务器。",
"@+CUDPSCLOSE：关闭UDP服务器。",

#====================================================
#GPIO操作。
"@+CGPIO?=PinIndex：查询指定的GPIO端口状态值（0/1）。",
"@+CGPIO=PinIndex，Value：设置指定的GPIO端口状态值。",
]

def xcmd_list():
	for strCMD in strCMDList:
		print strCMD
	#for i in range(len(strCMDList)):
	#	print strCMDList[i]
	
#============================================
if __name__ == "__main__":
	xcmd_list()

