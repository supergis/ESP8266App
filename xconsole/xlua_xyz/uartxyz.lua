local ximu = {}

function ximu.start()
    uart.on("data", "\r", 
        function(data)
    		if string.sub(data,1,1)=="\n" then
    		  data = string.sub(data,2)
    		end
    		local sub_str = string.sub(data, 1, 4)
    		print("CMD:"..sub_str)
    		if sub_str=="#YPR" then
        		print("XYZ: "..data)
           		zserver.send(data)
    		end
 
    		if string.sub(data,1,4)=="quit" then 
              uart.on("data")
              print("now in lua console.")
            end 
    	end, 0)
end

return ximu
