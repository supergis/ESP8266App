#!/usr/bin/env python 
import threading 
from socket import *
from time import ctime

#HOST = '192.168.199.227'
#PORT = 8268
HOST = '192.168.199.180'
#HOST = '192.168.1.8'
PORT = 8080

BUFSIZ = 1024 
ADDR = (HOST, PORT) 
IS_RUN = 1

def asocket():
	while True: 
		if IS_RUN == 0:
			break
		data = tcpCliSock.recv(1024)
		if not data: 
		    break 
		print ">>",data.strip()

if __name__ == "__main__":
	print "Connect to TCP server...EXIT to exit."
	tcpCliSock = socket(AF_INET,SOCK_STREAM)
	tcpCliSock.connect(ADDR) 

	print "Start receive thread..."
	t_asocket = threading.Thread(target=asocket,args=())
	t_asocket.setDaemon(True)
	t_asocket.start()

	while True: 
		indata = raw_input('>') 
		if (indata.upper()=="EXIT"):
			IS_RUN = 0
			tcpCliSock.close()
			print "exited."
			exit()
		tcpCliSock.send(indata)

