#====================================================
#WiFi工作模式设置。
#1-Station，2-softAP，3-softAP+Station

#返回WiFi的工作模式。
@+CWMODE?

#设置WiFi的工作模式。
@+CWMODE=1/2/3

#返回WiFi的工作频道。
@+CWCHANNEL?

#设置WiFi的工作频道。
@+CWCHANNEL=0～13
	
#====================================================
#设置无线路由器连接信息。

#扫描可用的热点。
@+CWLAP

#返回接入点SSID和PWD。
@+CWJAP?

#设置接入点SSID和PWD。
@+CWJAP=APPWD

#退出当前AP的连接。
@+CWQAP

#====================================================
#softAP设置信息。

#作为AP的参数查询。
@+CWSAP?

#作为AP的参数设置。
@+CWSAP=SSID,PWD,CHANNEL,AUTHMODE

#查看已接入设备的IP。
@+CWLIF

