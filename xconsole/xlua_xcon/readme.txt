=====================================================
xconsole for nodemcu.
using python to control nodemcu,interactive or batch.
-----------------------------------------------------
Author: openthings@163.com. copyright&GPL V2.
Last modified 2014-11-15.

Thanks for any suggestion.
=====================================================

************
Quick start:
************
Interactive console:
python xconsole.py,(may need change the serial port.)
print("hello,world.")

Uploadfile:
Using load_lua.py,change the filename inside.

Batch execute:
run other python as your local function.
examples under .\test\*.


