#!/usr/bin/env python
#coding=utf-8

#This file for nodemcu, maked by ZeroDay.
#Using this to automate the discovery procedure.
#Author: openthings@163.com. copyright&GPL V2.
#Last modified 2014-11-15.

import sys
import time
import x_serial
import x_mcu
import x_file

#============================================
#上载lua文件到nodemcu的存储区。
#Upload lua file to nodemcu.
#============================================

#Which file to upload.

#===============================================
print "========================================="
print "Willcome xconsole for nodeMCU."
print "by openthings@163.com. copyright&GPL V2."
print "========================================="

x_serial.isSimulate = 0
x_serial.port = '/dev/ttyUSB1'
x_serial.bandrate = 9600
x_serial.serialopen()
print "Open Serial at: ",x_serial.port,",",x_serial.baudrate

#处理命令行参数:本地文件名，板上文件名。
#Get param from args:hostfile,lua filename on board.
if len(sys.argv)==1:
	luafile_host = "./l_init.lua"
	luafile = "init.lua"
elif len(sys.argv)==3:
	luafile_host = sys.argv[1]
	luafile = sys.argv[2]
else:
	print "Usage: load_lua.py luafilehost luafilename"
	exit()

print "Uploading lua file to nodemcu..."
print "From:",luafile_host,"To:",luafile
x_file.uploadfile(luafile_host,luafile)
print ""

#print "Will restarting..."
#strLast = x_mcu.restart()
#x_serial.waitstart()

x_serial.serialclose()
print "xconsole Closed."
#===============================================

