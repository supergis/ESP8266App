#!/usr/bin/env python
#coding=utf-8

#This is a x file for nodemcu, maked by ZeroDay.
#Using this to automate the test procedure.
#Author: openthings@163.com. copyright&GPL V2.
#Last modified 2014-10-17.

import x_serial

#描述：重新启动
def restart():
	x_serial.writeln("node.restart()")
	
#描述：深度睡眠 us 微秒，时间到之后将重新启动
#us：时间，单位微秒
def dsleep(us):
	strResult = x_serial.xcmd2("node.dsleep(us)")
	return strResult

#描述：返回芯片id
#返回：number
def chipid():
	strResult = x_serial.xcmd2("node.chipid()")
	return strResult

#描述：返回可用内存 in bytes
#返回：系统heap剩余
def heap():
	strResult = x_serial.xcmd2("node.heap()")
	return strResult


