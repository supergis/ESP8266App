#!/usr/bin/env python
#This is a test file for nodemcu, maked by ZeroDay.
#Using this to automate the test procedure.
#Author: openthings@163.com. copyright&GPL V2.
#Last modified 2014-10-15.

import time
import x_serial
import x_mcu

x_serial.isSimulate = 0
x_serial.port = '/dev/ttyUSB2'
x_serial.bandrate = 9600
x_serial.serialopen()
print "   Open Serial at: ",x_serial.port,",",x_serial.baudrate

x_serial.sayhello()

x_serial.serialclose()		
print "xconsole Closed."
