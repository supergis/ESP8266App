#!/usr/bin/env python
#This is a test file for nodemcu, maked by ZeroDay.
#Using this to automate the test procedure.
#Author: openthings@163.com. copyright&GPL V2.
#Last modified 2014-10-17.

import time
import x_serial
import x_mcu

x_serial.isSimulate = 0

strLast = x_mcu.chipid()
print "CHIPID:",strLast

strLast = x_mcu.heap()
print "HEAP:",strLast


strLast = x_mcu.format()
print "FORMAT:",strLast

strLast = x_mcu.startlog("init.lua","0")
print "LOGSTART:",strLast

strLast = x_serial.xwrite("print(\"Hello,World,wowo\")")
#strLast = x_serial.xwrite("print(\"I am born from python.\")")

strLast = x_mcu.stoplog()
print "LOGSTOP:",strLast

strLast = x_mcu.restart()
print "RESTART:",strLast

#list

strLast = x_mcu.readlog("init.lua")
print "READLOG:",strLast

x_serial.serialclose()

