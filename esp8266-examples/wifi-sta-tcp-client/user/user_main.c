#include "ets_sys.h"
#include "osapi.h"
#include "os_type.h"
#include "user_interface.h"
#include "driver/uart.h"
#include "espconn.h"
#include "mem.h"
#include "gpio.h"
#include "user_config.h"

typedef enum {
	WIFI_CONNECTING,
	WIFI_CONNECTING_ERROR,
	WIFI_CONNECTED,
	TCP_DISCONNECTED,
	TCP_CONNECTING,
	TCP_CONNECTING_ERROR,
	TCP_CONNECTED
} tConnState;

static char macaddr[6];
static ETSTimer BtnTimer;
static ETSTimer WiFiLinker;
uint16_t GPIO_Time_Active = 0;
static tConnState connState = WIFI_CONNECTING;

static void ICACHE_FLASH_ATTR
at_tcpclient_sent_cb(void *arg) {
	#ifdef PLATFORM_DEBUG
	uart0_sendStr("Send callback\r\n");
	#endif
	struct espconn *pespconn = (struct espconn *)arg;
	espconn_disconnect(pespconn);
}

static void ICACHE_FLASH_ATTR
at_tcpclient_discon_cb(void *arg) {
	struct espconn *pespconn = (struct espconn *)arg;
	os_free(pespconn->proto.tcp);
	os_free(pespconn);
	#ifdef PLATFORM_DEBUG
	uart0_sendStr("Disconnect callback\r\n");
	#endif
}

static void ICACHE_FLASH_ATTR
at_tcpclient_connect_cb(void *arg)
{
	struct espconn *pespconn = (struct espconn *)arg;
	#ifdef PLATFORM_DEBUG
	uart0_sendStr("TCP client connect\r\n");
	#endif
	espconn_regist_sentcb(pespconn, at_tcpclient_sent_cb);
	//espconn_regist_recvcb(pespconn, at_tcpclient_recv);
	espconn_regist_disconcb(pespconn, at_tcpclient_discon_cb);
	char payload[128];
	os_sprintf(payload, MACSTR ",%s\r\n", MAC2STR(macaddr), "ESP8266");
	#ifdef PLATFORM_DEBUG
	uart0_sendStr(payload);
	#endif
	espconn_sent(pespconn, payload, strlen(payload));
}

static void ICACHE_FLASH_ATTR
senddata()
{
	char info[150];
	char tcpserverip[15];
	struct espconn *pCon = (struct espconn *)os_zalloc(sizeof(struct espconn));
	if (pCon == NULL)
	{
		#ifdef PLATFORM_DEBUG
		uart0_sendStr("TCP connect failed\r\n");
		#endif
		return;
	}
	pCon->type = ESPCONN_TCP;
	pCon->state = ESPCONN_NONE;
	os_sprintf(tcpserverip, "%s", TCPSERVERIP);
	uint32_t ip = ipaddr_addr(tcpserverip);
	pCon->proto.tcp = (esp_tcp *)os_zalloc(sizeof(esp_tcp));
	pCon->proto.tcp->local_port = espconn_port();
	pCon->proto.tcp->remote_port = TCPSERVERPORT;
	os_memcpy(pCon->proto.tcp->remote_ip, &ip, 4);
	espconn_regist_connectcb(pCon, at_tcpclient_connect_cb);
	//espconn_regist_reconcb(pCon, at_tcpclient_recon_cb);
	#ifdef PLATFORM_DEBUG
	os_sprintf(info,"Start espconn_connect to " IPSTR ":%d\r\n",
		   IP2STR(pCon->proto.tcp->remote_ip),
		   pCon->proto.tcp->remote_port);
	uart0_sendStr(info);
	#endif
	espconn_connect(pCon);
}

static void ICACHE_FLASH_ATTR BtnTimerCb(void *arg)
{
	if (!GPIO_INPUT_GET(BTNGPIO))
	{
		GPIO_Time_Active++;
	} else {
		if (GPIO_Time_Active != 0)
		{
			#ifdef PLATFORM_DEBUG
			uart0_sendStr("Start sending data...\r\n");
			#endif
			senddata();
		}
		GPIO_Time_Active = 0;
	}
}

void BtnInit() {
	// Select pin function
	PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO0_U, FUNC_GPIO0);
	// Disable pulldown
	PIN_PULLDWN_DIS(PERIPHS_IO_MUX_GPIO0_U);
	// Enable pull up R
	PIN_PULLUP_EN(PERIPHS_IO_MUX_GPIO0_U);  
	// Set GPIO0 as input mode
	gpio_output_set(0, 0, 0, BIT0);
	os_timer_disarm(&BtnTimer);
	os_timer_setfn(&BtnTimer, BtnTimerCb, NULL);
	os_timer_arm(&BtnTimer, 500, 1);
}

static void ICACHE_FLASH_ATTR wifi_check_ip(void *arg)
{
	struct ip_info ipConfig;

	os_timer_disarm(&WiFiLinker);
	wifi_get_ip_info(STATION_IF, &ipConfig);
	if (wifi_station_get_connect_status() == STATION_GOT_IP && ipConfig.ip.addr != 0)
	{
		connState = WIFI_CONNECTED;
		#ifdef PLATFORM_DEBUG
	        uart0_sendStr("WiFi connected\r\n");
        #endif
		#ifdef PLATFORM_DEBUG
	        uart0_sendStr("Start TCP connecting...\r\n");
        #endif
		connState = TCP_CONNECTING;
		senddata();
		os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);
		os_timer_arm(&WiFiLinker, 5000, 0);
	}
	else
	{
		if(wifi_station_get_connect_status() == STATION_WRONG_PASSWORD)
		{
			connState = WIFI_CONNECTING_ERROR;
			#ifdef PLATFORM_DEBUG
			uart0_sendStr("WiFi connecting error, wrong password\r\n");
			#endif
			os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);
			os_timer_arm(&WiFiLinker, 1000, 0);
		}
		else if(wifi_station_get_connect_status() == STATION_NO_AP_FOUND)
		{
			connState = WIFI_CONNECTING_ERROR;
			#ifdef PLATFORM_DEBUG
			uart0_sendStr("WiFi connecting error, ap not found\r\n");
			#endif
			os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);
			os_timer_arm(&WiFiLinker, 1000, 0);
		}
		else if(wifi_station_get_connect_status() == STATION_CONNECT_FAIL)
		{
			connState = WIFI_CONNECTING_ERROR;
			#ifdef PLATFORM_DEBUG
			uart0_sendStr("WiFi connecting fail\r\n");
			#endif
			os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);
			os_timer_arm(&WiFiLinker, 1000, 0);
		}
		else
		{
			os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);
			os_timer_arm(&WiFiLinker, 1000, 0);
			connState = WIFI_CONNECTING;
			#ifdef PLATFORM_DEBUG
			uart0_sendStr("WiFi connecting...\r\n");
			#endif
		}
	}
}

//Init function 
void ICACHE_FLASH_ATTR
user_init()
{
	uart_init(BIT_RATE_115200, BIT_RATE_115200);
	os_delay_us(100);

	#ifdef PLATFORM_DEBUG
	uart0_sendStr("ESP8266 platform starting...\r\n");
	#endif
    
	struct station_config stationConfig;
	char info[150];

	if(wifi_get_opmode() != STATION_MODE)
	{
		#ifdef PLATFORM_DEBUG
		uart0_sendStr("ESP8266 not in STATION mode, restarting in STATION mode...\r\n");
		#endif
		wifi_set_opmode(STATION_MODE);
		//after esp_iot_sdk_v0.9.2, need not to restart
		//system_restart();
	}

	if(wifi_get_opmode() == STATION_MODE)
	{
		wifi_station_get_config(&stationConfig);
		os_memset(stationConfig.ssid, 0, sizeof(stationConfig.ssid));
		os_memset(stationConfig.password, 0, sizeof(stationConfig.password));
		os_sprintf(stationConfig.ssid, "%s", WIFI_CLIENTSSID);
		os_sprintf(stationConfig.password, "%s", WIFI_CLIENTPASSWORD);
		wifi_station_set_config(&stationConfig);
		wifi_get_macaddr(SOFTAP_IF, macaddr);
	}

	#ifdef PLATFORM_DEBUG
	if(wifi_get_opmode() == STATION_MODE)
	{
		wifi_station_get_config(&stationConfig);
		os_sprintf(info,"OPMODE: %u, SSID: %s, PASSWORD: %s\r\n",
			wifi_get_opmode(),
			stationConfig.ssid,
			stationConfig.password);
		uart0_sendStr(info);
	}
	#endif
	// Wait for WIFI connection and start TCP connection
	os_timer_disarm(&WiFiLinker);
	os_timer_setfn(&WiFiLinker, (os_timer_func_t *)wifi_check_ip, NULL);
	os_timer_arm(&WiFiLinker, 1000, 0);

	BtnInit();

	#ifdef PLATFORM_DEBUG
	uart0_sendStr("ESP8266 platform started!\r\n");
	#endif
}

