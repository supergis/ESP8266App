static int
nmea_tokenizer_init( NmeaTokenizer*  t, const char*  p, const char*  end )
{
     int    count = 0;
    char*  q;
    // the initial '$' is optional
    if (p < end && p[0] == '$')
         p += 1;

     // remove trailing newline
     if (end > p && end[-1] == '\n') {
         end -= 1;
136         if (end > p && end[-1] == '\r')
137             end -= 1;
138     }
139 
140     // get rid of checksum at the end of the sentecne
141     if (end >= p+3 && end[-3] == '*') {
142         end -= 3;
143     }
144 
145     while (p < end) {
146         const char*  q = p;
147 
148         q = memchr(p, ',', end-p);
149         if (q == NULL)
150             q = end;
151 
152         if (count < MAX_NMEA_TOKENS) {
153             t->tokens[count].p   = p;
154             t->tokens[count].end = q;
155             count += 1;
156         }
157 
158         if (q < end)
159             q += 1;
160 
161         p = q;
162     }
163 
     t->count = count;
    return count;
 }

if ( !memcmp(tok.p, "GGA", 3) ) {
512         // GPS fix
513         Token  tok_fixstaus      = nmea_tokenizer_get(tzer,6);
514 
515         if (tok_fixstaus.p[0] > '0') {
516 
517           Token  tok_time          = nmea_tokenizer_get(tzer,1);  //token 1 代表的是时间
518           Token  tok_latitude      = nmea_tokenizer_get(tzer,2); //token 2 代表的是纬度
519           Token  tok_latitudeHemi  = nmea_tokenizer_get(tzer,3); 
520           Token  tok_longitude     = nmea_tokenizer_get(tzer,4); //token 4 经度
521           Token  tok_longitudeHemi = nmea_tokenizer_get(tzer,5);
522           Token  tok_altitude      = nmea_tokenizer_get(tzer,9); //altitude
523           Token  tok_altitudeUnits = nmea_tokenizer_get(tzer,10);
524 //更新时间，经度纬度
525           nmea_reader_update_time(r, tok_time);
526           nmea_reader_update_latlong(r, tok_latitude,
527                                         tok_latitudeHemi.p[0],
528                                         tok_longitude,
529                                         tok_longitudeHemi.p[0]); 
530           nmea_reader_update_altitude(r, tok_altitude, tok_altitudeUnits);
531         }
532 
533     }


