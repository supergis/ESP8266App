#!/bin/bash

export PATH=/home/supermap/OpenThings/esp-open-sdk/xtensa-lx106-elf/bin:$PATH
echo $PATH

echo "================================="
echo "git all for ESP8266."
echo "================================="

#echo "1-Please git ESP8266App on parent, run as: "
#echo "git clone https://git.oschina.net/supergis/ESP8266App"

#echo "2-git esp-open-sdk."
#git clone https://github.com/pfalcon/esp-open-sdk.git

echo "===================================================="
echo "2-git esp_mqtt_sensor."
git clone https://github.com/supergis/esp_mqtt_sensor.git

echo "===================================================="
echo "2B-git espduino."
git clone https://github.com/supergis/espduino.git

echo "===================================================="
echo "3-git esp_mqtt_oled."
git clone https://github.com/supergis/esp_mqtt_oled.git

#From Espressif offically.
echo "===================================================="
echo "4-git rtos-sdk-lib-espressif of esp8266."
git clone https://github.com/espressif/esp_iot_rtos_sdk_lib.git

echo "===================================================="
echo "4A-git rtos-espressif of esp8266."
git clone https://github.com/supergis/esp_iot_rtos_sdk.git

echo "===================================================="
echo "4B-git at-espressif of esp8266."
git clone https://github.com/supergis/esp8266_at.git
#cd ..

echo "===================================================="
echo "4C-git low_power-espressif of esp8266."
git clone https://github.com/supergis/low_power_voltage_measurement.git
#cd ..
#End from Espressif offically.

echo "===================================================="
echo "6-git micropython for esp8266."
git clone https://github.com/supergis/micropython.git

echo "===================================================="
echo "7-git nodemcu for esp8266."
mkdir nodemcu
cd nodemcu
git clone https://github.com/supergis/nodemcu-firmware.git
cd ..

echo "===================================================="
echo "8-git nodelua."
git clone https://github.com/supergis/nodelua.git

echo "===================================================="
echo "9-git frankenstein."
git clone https://github.com/supergis/esp8266-frankenstein.git

echo "Git clone for ESP8266, all ok."

