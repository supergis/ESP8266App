import sublime, sublime_plugin

class ExampleCommand(sublime_plugin.TextCommand):
  def run(self, edit):
      #self.view.insert(edit, 0, "Hello, World!")
      print("Hello, World")
      newlua.newlua()

def filecopy(filenamein,filenameout):	
	fin = open(filenamein, 'r+')
	fout = open(filenameout, 'w+')
	for  strline in  fin: 
		#add filter here.
		fout.write(strline)
	fin.close()
	fout.close()


class ShowESP8266MenuCommand(sublime_plugin.WindowCommand):
	def run(self):
		show_ESP8266_menu = not app.constant.global_settings.get('show_ESP8266_menu', True)
		app.constant.global_settings.set('show_ESP8266_menu', show_ESP8266_menu)
		app.main_menu.refresh()

	def is_checked(self):
		state = app.constant.global_settings.get('show_ESP8266_menu', True)
		return state

class NewLuaFileCommand(sublime_plugin.WindowCommand):
	def run(self):
		self.window.open_file(sublime.packages_path()+"/esp/lua/newluafile.lua")

class NewLuaModuleCommand(sublime_plugin.WindowCommand):
	def run(self):
		self.window.open_file(sublime.packages_path()+"/esp/lua/newluamodule.lua")

class NewLuaInitCommand(sublime_plugin.WindowCommand):
	def run(self):
		self.window.open_file(sublime.packages_path()+"/esp/lua/newluainit.lua")

class NewSketchCommand(sublime_plugin.WindowCommand):
	def run(self):
		self.window.open_file(sublime.packages_path()+"/esp/lua/newluainit.lua")
		self.window.open_file(sublime.packages_path()+"/esp/lua/newluafile.lua")
		self.window.open_file(sublime.packages_path()+"/esp/lua/newluamodule.lua")		


