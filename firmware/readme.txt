This directory contain firmware of latest build.
By openthings@163.com.

Firmware:
1. esp.nodemcu.bin,OK，elua的实现，功能全面，稳定可用。
2. esp.nodelua.bin,OK，elua的实现。
3. esp.wixcmd.bin,OK，xcmd的实现，透传专用。
4. esp.mpython.bin,OK，micropython的实现。
5. esp.rtos.bin,OK，官方的RtOS实现。
6. esp.at.bin,官方AT指令集的实现，编译未通过。
7. esp.mqtt.bin,OK，MQTT的移植。
8. esp.frankenstein.bin,OK,仿linux的交互控制台。


Flash to ESP8266 module:
Please using esp.*.sh to flash(pull LOW GPIO0/GPIO15,and CH_PD to HIGH), may need change the port.
On Mac:using ls /dev/tty.* to list available uart port.
On Linux:using dmesg to view available usb uart port.

