#!/bin/bash

echo "================================="
echo "commit all source to oschina..."
echo "================================="

echo "1-commit and push ESP8266App."
git add ./ -A
git commit -a -m "update ..."
git push

echo "===================================================="
echo "2-commit and push esp_mqtt."
cd esp_mqtt
git add ./ -A
git commit -a -m "update ..."
git push
cd ..

echo "===================================================="
echo "2B-commit and push espduino."
cd espduino
git add ./ -A
git commit -a -m "update ..."
git push
cd ..

echo "===================================================="
echo "3-commit and push esp_mqtt_oled."
cd esp_mqtt_oled
git add ./ -A
git commit -a -m "update ..."
git push
cd ..

echo "===================================================="
echo "4A-commit and push esp_iot_rtos_sdk."
cd esp_iot_rtos_sdk
git add ./ -A
git commit -a -m "update ..."
git push
cd ..

echo "===================================================="
echo "4B-commit and push esp8266_at."
cd esp8266_at
git add ./ -A
git commit -a -m "update ..."
git push
cd ..

echo "===================================================="
echo "4C-commit and push low_power-espressif."
cd low_power_voltage_measurement
git add ./ -A
git commit -a -m "update ..."
git push
cd ..

echo "===================================================="
echo "6-commit and push micropython for esp8266."
cd micropython
git add ./ -A
git commit -a -m "update ..."
git push
cd ..

echo "===================================================="
echo "7-commit and push nodemcu."
cd nodemcu/nodemcu-firmware
git add ./ -A
git commit -a -m "update ..."
git push
cd ../../

echo "===================================================="
echo "8-commit and push nodelua."
cd nodelua
git add ./ -A
git commit -a -m "update ..."
git push
cd ..

echo "===================================================="
echo "9-commit and push frankenstein."
cd esp8266-frankenstein
git add ./ -A
git commit -a -m "update ..."
git push
cd ..

