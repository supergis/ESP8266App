#!/bin/bash

echo "================================="
echo "Build all for ESP8266."
echo "================================="

cd ./build

echo "===================================================="
echo "1、Build wixcmd."
./build_wixcmd.sh

echo "===================================================="
echo "2、Build esp_mqtt_sensor."
./build_mqtt_sensor.sh

echo "===================================================="
echo "3、Build esp_mqtt_oled."
./build_mqtt_oled.sh

echo "===================================================="
echo "4A、Build rtos."
./build_rtos.sh

echo "===================================================="
echo "4B、Build AT."
./build_at.sh

echo "===================================================="
echo "4C、Build lowpower."
./build_lowpower.sh

echo "===================================================="
echo "6、Build micropython."
./build_mpy.sh

echo "===================================================="
echo "7、Build nodemcu."
./build_nmcu.sh

echo "===================================================="
echo "8、Build nodelua."
./build_nlua.sh

echo "===================================================="
echo "9、Build frankenstein."
./build_frank.sh

cd ..
echo "================================="
echo "Build all for ESP8266 is OK."
echo "Plase check firmware."
echo "================================="


